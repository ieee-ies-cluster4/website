# IEEE-IES Cluster 4 (Cross-Disciplinary)

Public website for the IEEE-IES Cluster 4 (Cross-Disciplinary)

## Contents

[TOC]

# Contributing

## Contributing to this document

Members of the IEEE-IES Cluster 4 are expected to contribute to this document. The idea is to have a public list of the ongoing collaborations, so these can be discovered by other interested parties.

This is a public repository so only non-confidential information should be shared here. Per-group or per-project access can be given to collaborators.

This document uses [Gitlab-flavored Markdown syntax](https://docs.gitlab.com/ee/user/markdown.html)

## Contributing to a listed project

See a project that you want to contribute to? Please follow the 'How to contribute' instructions for the project that you want to join.

# What is the IEEE-IES Cluster 4?

The Industrial Electronics Society (IES) of IEEE groups its [Technical Commitees](http://www.ieee-ies.org/technical-committees/28-members/adcom-tcs) (TCs) into Clusters.

Due to the cross-disciplinary nature of the Cluster, the mission of Cluster 4 in not only to coordinate its TCs, but also to foster collaboration between different disciplines.

Cluster 4 includes the following Technical Committees:

- [Education in Engineering and Industrial Technologies (EEIT)](https://sites.google.com/view/05-committee-on-education/)
- [Electronic Systems on Chip (ESoC)](https://esoc.ieee-ies.org/)
- [Human Factors (HF)](https://sites.google.com/view/ieee-ies-tc-hf)
- [Resilience and Security for Industrial Applications (ReSIA)](https://sites.google.com/ncsu.edu/ieee-ies-tc-resia/about-us)
- [Standards](https://sites.google.com/view/iesstandardstc/)
- [Technology Ethics and Society (TES)](https://tes.ieee-ies.org/)


# On-going Collaborations

... List current projects here...

## Example project

- What
- When
- How to contribute: open an issue on this project and assign it to @hipolitoguzman


# Completed Collaborations

- [ ] Completed collaborations can be first moved to this section of the document and, when the document grows, moved to a different ``.md`` file
